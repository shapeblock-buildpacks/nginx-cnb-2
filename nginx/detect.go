package node

import (
	"io/ioutil"
	"path/filepath"

	"github.com/paketo-buildpacks/packit"
	"gopkg.in/yaml.v2"
)

type Location struct {
	Root    string   `yaml:"root"`
	Index   []string `yaml:"index"`
	Scripts bool     `yaml:"scripts"`
	Allow   bool     `yaml:"allow"`
}

type PlatformConfig struct {
	Name  string `yaml:"name"`
	Type  string `yaml:"type"`
	Build struct {
		Flavor string `yaml:"flavor"`
	} `yaml:"build,omitempty"`
	Web struct {
		Commands struct {
			Start string `yaml:"start"`
		} `yaml:"commands"`
		Locations map[string]Location `yaml:"locations"`
	} `yaml:"web"`
	X map[string]interface{} `yaml:"-"`
}

func Detect() packit.DetectFunc {
	return func(context packit.DetectContext) (packit.DetectResult, error) {

		yamlFile, err := ioutil.ReadFile(filepath.Join(context.WorkingDir, ".platform.app.yaml"))
		if err != nil {
			return packit.DetectResult{}, err
		}
		var config PlatformConfig

		err = yaml.Unmarshal(yamlFile, &config)
		if err != nil {
			return packit.DetectResult{}, err
		}

		return packit.DetectResult{
			Plan: packit.BuildPlan{
				Provides: []packit.BuildPlanProvision{
					{Name: "node-start"},
				},
				Requires: []packit.BuildPlanRequirement{
					{
						Name: "node-start",
					},
				},
			},
		}, nil
	}
}
