package node

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"text/template"

	"github.com/paketo-buildpacks/packit"
	"gopkg.in/yaml.v2"
)

type NginxConfig struct {
	Docroot string
}

const NginxConfTemplate = `daemon     off;
error_log  stderr  notice;
worker_processes  auto;
events {
    worker_connections  1024;
}
http {
    types {
        text/html                             html htm shtml;
        text/css                              css;
        text/xml                              xml;
        image/gif                             gif;
        image/jpeg                            jpeg jpg;
        application/javascript                js;
        application/atom+xml                  atom;
        application/rss+xml                   rss;
        text/mathml                           mml;
        text/plain                            txt;
        text/vnd.sun.j2me.app-descriptor      jad;
        text/vnd.wap.wml                      wml;
        text/x-component                      htc;
        image/png                             png;
        image/tiff                            tif tiff;
        image/vnd.wap.wbmp                    wbmp;
        image/x-icon                          ico;
        image/x-jng                           jng;
        image/x-ms-bmp                        bmp;
        image/svg+xml                         svg svgz;
        image/webp                            webp;
        application/font-woff                 woff;
        application/java-archive              jar war ear;
        application/json                      json;
        application/mac-binhex40              hqx;
        application/msword                    doc;
        application/pdf                       pdf;
        application/postscript                ps eps ai;
        application/rtf                       rtf;
        application/vnd.ms-excel              xls;
        application/vnd.ms-fontobject         eot;
        application/vnd.ms-powerpoint         ppt;
        application/vnd.wap.wmlc              wmlc;
        application/vnd.google-earth.kml+xml  kml;
        application/vnd.google-earth.kmz      kmz;
        application/x-7z-compressed           7z;
        application/x-cocoa                   cco;
        application/x-java-archive-diff       jardiff;
        application/x-java-jnlp-file          jnlp;
        application/x-makeself                run;
        application/x-perl                    pl pm;
        application/x-pilot                   prc pdb;
        application/x-rar-compressed          rar;
        application/x-redhat-package-manager  rpm;
        application/x-sea                     sea;
        application/x-shockwave-flash         swf;
        application/x-stuffit                 sit;
        application/x-tcl                     tcl tk;
        application/x-x509-ca-cert            der pem crt;
        application/x-xpinstall               xpi;
        application/xhtml+xml                 xhtml;
        application/zip                       zip;
        application/octet-stream              bin exe dll;
        application/octet-stream              deb;
        application/octet-stream              dmg;
        application/octet-stream              iso img;
        application/octet-stream              msi msp msm;
        application/vnd.openxmlformats-officedocument.wordprocessingml.document    docx;
        application/vnd.openxmlformats-officedocument.spreadsheetml.sheet          xlsx;
        application/vnd.openxmlformats-officedocument.presentationml.presentation  pptx;
        audio/midi                            mid midi kar;
        audio/mpeg                            mp3;
        audio/ogg                             ogg;
        audio/x-m4a                           m4a;
        audio/x-realaudio                     ra;
        video/3gpp                            3gpp 3gp;
        video/mp4                             mp4;
        video/mpeg                            mpeg mpg;
        video/quicktime                       mov;
        video/webm                            webm;
        video/x-flv                           flv;
        video/x-m4v                           m4v;
        video/x-mng                           mng;
        video/x-ms-asf                        asx asf;
        video/x-ms-wmv                        wmv;
        video/x-msvideo                       avi;
    }
    default_type       application/octet-stream;
    sendfile           on;
    keepalive_timeout  65;
    gzip               on;
    port_in_redirect   off;
    root               /workspace/{{ .Docroot }};
    index              index.php index.html;
    server_tokens      off;
    log_format common '$remote_addr - $remote_user [$time_local] "$request" $status $body_bytes_sent';
    log_format extended '$remote_addr - $remote_user [$time_local] "$request" $status $body_bytes_sent vcap_request_id=$http_x_vcap_request_id';
    access_log  /dev/stdout  extended;
    # set $https only when SSL is actually used.
    map $http_x_forwarded_proto $proxy_https {
        https on;
    }
    # setup the scheme to use on redirects
    map $http_x_forwarded_proto $redirect_scheme {
        default http;
        http http;
        https https;
    }
    # map conditions for redirect
    map $http_x_forwarded_proto $redirect_to_https {
        default no;
        http yes;
        https  no;
    }
    server {
        listen       8080  default_server;
        server_name localhost;
        fastcgi_temp_path      /tmp/nginx_fastcgi 1 2;
        client_body_temp_path  /tmp/nginx_client_body 1 2;
        proxy_temp_path        /tmp/nginx_proxy 1 2;
        real_ip_header         x-forwarded-for;
        set_real_ip_from       10.0.0.0/8;
        real_ip_recursive      on;
        # forward http to https
        if ($redirect_to_https = "yes") {
            return 301 https://$http_host$request_uri;
        }
        # Allow "Well-Known URIs" as per RFC 8615
        location ~* ^/.well-known/ {
            allow all;
        }
        
        # Deny hidden files (.htaccess, .htpasswd, .DS_Store)
        location ~ /\. {
            deny            all;
            access_log      off;
            log_not_found   off;
        }
        # Some basic cache-control for static files to be sent to the browser
        location ~* \.(?:ico|css|js|gif|jpeg|jpg|png)$ {
            expires         max;
            add_header      Pragma public;
            add_header      Cache-Control "public, must-revalidate, proxy-revalidate";
        }
	}
}
`

func WriteFileFromReader(filename string, perm os.FileMode, source io.Reader) error {
	if err := os.MkdirAll(filepath.Dir(filename), 0755); err != nil {
		return err
	}

	f, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, perm)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = io.Copy(f, source)
	if err != nil {
		return err
	}

	return nil
}

func ProcessTemplateToFile(templateBody string, outputPath string, data interface{}) error {
	template, err := template.New(filepath.Base(outputPath)).Parse(templateBody)
	if err != nil {
		return err
	}

	var b bytes.Buffer
	err = template.Execute(&b, data)
	if err != nil {
		return err
	}

	return WriteFileFromReader(outputPath, 0644, &b)
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func Build() packit.BuildFunc {
	return func(context packit.BuildContext) (packit.BuildResult, error) {

		nodeStartLayer, err := context.Layers.Get("node-start")
		if err != nil {
			return packit.BuildResult{}, err
		}

		yamlFile, err := ioutil.ReadFile(filepath.Join(context.WorkingDir, ".platform.app.yaml"))
		if err != nil {
			return packit.BuildResult{}, err
		}
		var config PlatformConfig

		err = yaml.Unmarshal(yamlFile, &config)
		if err != nil {
			return packit.BuildResult{}, err
		}
		startCmd := config.Web.Commands.Start
		if startCmd == "" {

			nginxBin := filepath.Join(nodeStartLayer.Path, "sbin", "nginx")
			if !fileExists(nginxBin) {
				fmt.Printf("Installing nginx\n")

				nodeStartLayer, err = nodeStartLayer.Reset()
				if err != nil {
					return packit.BuildResult{}, err
				}

				uri := "https://buildpacks.cloudfoundry.org/dependencies/nginx/nginx_1.18.0_linux_x64_cflinuxfs3_10435f5f.tgz"
				downloadDir, err := ioutil.TempDir("", "downloadDir")
				if err != nil {
					return packit.BuildResult{}, err
				}
				defer os.RemoveAll(downloadDir)

				fmt.Println("Downloading dependency...")
				err = exec.Command("curl",
					uri,
					"-o", filepath.Join(downloadDir, "nginx.tar.xz"),
				).Run()
				if err != nil {
					return packit.BuildResult{}, err
				}

				fmt.Println("Untaring dependency...")
				cmd := exec.Command("tar",
					"-xf",
					filepath.Join(downloadDir, "nginx.tar.xz"),
					"--strip-components=1",
					"-C", nodeStartLayer.Path,
				)
				cmd.Stdout = os.Stdout
				cmd.Stderr = os.Stderr
				if err = cmd.Run(); err != nil {
					return packit.BuildResult{}, err
				}
				fmt.Printf("Layer path -> %s\n", nodeStartLayer.Path)

				nginxConf := filepath.Join(nodeStartLayer.Path, "conf", "nginx.conf")
				cfg := NginxConfig{
					Docroot: config.Web.Locations["/"].Root,
				}
				fmt.Printf("Locations -> %v\n", config.Web.Locations)
				err = ProcessTemplateToFile(NginxConfTemplate, nginxConf, cfg)
				if err != nil {
					return packit.BuildResult{}, err
				}
			}
			startCmd = fmt.Sprintf("%s/sbin/nginx -p %s -c %s/conf/nginx.conf", nodeStartLayer.Path, nodeStartLayer.Path, nodeStartLayer.Path)
		} else {
			fmt.Printf("Run command -> %s\n", startCmd)
			nodeStartLayer, err = nodeStartLayer.Reset()
			if err != nil {
				return packit.BuildResult{}, err
			}
			// Add dummy run file
			err = ioutil.WriteFile(filepath.Join(nodeStartLayer.Path, "run"), []byte(startCmd), 0644)
			if err != nil {
				return packit.BuildResult{}, err
			}
		}

		nodeStartLayer.Cache = true
		nodeStartLayer.Build = true
		nodeStartLayer.Launch = true
		return packit.BuildResult{
			Plan: context.Plan,
			Layers: []packit.Layer{
				nodeStartLayer,
			},
			Launch: packit.LaunchMetadata{
				Processes: []packit.Process{
					{
						Type:    "web",
						Command: startCmd,
					},
				},
			},
		}, nil
	}
}
