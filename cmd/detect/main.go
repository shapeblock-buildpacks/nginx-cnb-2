package main

import (
	nginx "gitlab.com/shapeblock-buildpacks/nginx-cnb-2/nginx"

	"github.com/paketo-buildpacks/packit"
)

func main() {
	packit.Detect(nginx.Detect())
}
